import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';
import { Header } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
  componentWillMount() {
    firebase.initializeApp({
      apiKey: 'AIzaSyBcJUwttU49epFnLt6oI-GBvVLx3WILsL8',
      authDomain: 'manager-deba0.firebaseapp.com',
      databaseURL: 'https://manager-deba0.firebaseio.com',
      projectId: 'manager-deba0',
      storageBucket: 'manager-deba0.appspot.com',
      messagingSenderId: '732541803465'
    });
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    return (
      <Provider store={store}>
        <View>
          <Header headerText="Auth with redux" />
          <LoginForm />
        </View>
      </Provider>
    );
  }
}

export default App;
