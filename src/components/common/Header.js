//Import libraries to help create a Component
import React from 'react';
import { Text, View } from 'react-native';

//Create a Component
const Header = (props) => {
  const { textStyle, viewStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textStyle}>{props.headerText} </Text>
    </View>
  );
};

const styles = {
    viewStyle: {
      backgroundColor: '#F8F8F8',
      justifyContent: 'center',
      alignItems: 'center',
      height: 60,
      padding: 15,
      borderBottomColor: '#000',
      borderBottomWidth: 0.5,
      elevation: 7,
      position: 'relative'
    },
    textStyle: {
      fontSize: 20
    }
};

//Making the Component available for other parts of the App
export { Header };
