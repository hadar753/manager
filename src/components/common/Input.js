import React from 'react';
import { TextInput, View, Text } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
  const { InputStyle, LabelStyle, containerStyle } = styles;

  return (
    <View style={containerStyle} >
      <Text style={LabelStyle}>{label}</Text>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        autoCorrect={false}
        label="Email"
        value={value}
        onChangeText={onChangeText}
        style={InputStyle}
      />
    </View>
  );
};

const styles = {
    InputStyle: {
      color: '#000',
      paddingRight: 5,
      paddingLeft: 5,
      fontSize: 20,
      lineHeight: 23,
      flex: 2
    },
    LabelStyle: {
      fontSize: 20,
      paddingLeft: 20,
      flex: 1
    },
    containerStyle: {
      height: 60,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center'
    }
};

export { Input };
