import React, { Component } from 'react';
import { connect } from 'react-redux';
import { CardSection, Button } from './common';
import { logOutUser } from '../actions';

class LogOutButton extends Component {

  onButtonPress() {
    this.props.logOutUser();
  }

  render() {
    return (
      <CardSection>
        <Button onPress={() => this.onButtonPress.bind(this)}>
          Log Out
        </Button>
      </CardSection>
    );
  }
}

export default connect(null, { logOutUser })(LogOutButton);
