const INITIAL_STATE = { username: '',
                        password: '',
                        user: null,
                        error: '',
                        loading: false,
                        loginStatus: 'logedout' };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'username_changed':
      return { ...state, username: action.payload };
    case 'password_changed':
      return { ...state, password: action.payload };
    case 'loggin_user_start':
      return { ...state, loading: true, error: '', loginStatus: 'unknown' };
    case 'loggin_user_success':
      return { ...state, ...INITIAL_STATE, user: action.payload, loginStatus: 'logedIn' };
    case 'loggin_user_fail':
      return { ...state,
              password: '',
              error: 'Authentication Faild.',
              loading: false,
              loginStatus: 'logedout' };
    case 'logout_user':
      return { ...state, user: null, loginStatus: 'logedout' };
    default:
      return state;
  }
};
