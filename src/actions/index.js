import firebase from 'firebase';

export const usernameChanged = (text) => {
  return {
    type: 'username_changed',
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: 'password_changed',
    payload: text
  };
};

export const loginUser = ({ username, password }) => {
  const email = username + '@test.com';
  return (dispatch) => {
    dispatch({ type: 'loggin_user_start' });
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(user => {
        dispatch({ type: 'loggin_user_success', payload: user });
      })
      .catch(() => {
        dispatch({ type: 'loggin_user_fail' });
      });
  };
};

export const logOutUser = () => {
  firebase.auth().signOut();
  return {
    type: 'logout_user'
  };
};
